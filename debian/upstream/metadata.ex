Bug-Submit: https://groups.io/g/swisseph
Documentation: https://www.astro.com/swisseph
Repository-Browse: https://github.com/aloistr/swisseph
Repository: https://github.com/aloistr/swisseph.git
